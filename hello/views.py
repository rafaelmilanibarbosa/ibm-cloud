from django.http import JsonResponse
import logging
import os

logger = logging.getLogger(__name__)


# Create your views here.
def hello(request):
    """
    Check status of each external service.
    Remember to keep everything lightweight and add short timeouts
    """
    result = {'status': 'ok'}
    logger.info('Performing hello endpoint')

    # Check DB making a lightweight DB query
    result['hello'] = {'message': str('Hello ') + str(os.environ.get('HELLO_USER_NAME'))}

    logger.debug('Hello result {}'.format(result))

    status_code = 200
    if result['status'] != 'ok':
        logger.error('Hello result is bad')
        status_code = 500
    else:
        logger.info('Hello result is ok')

    response = JsonResponse(result)
    response.status_code = status_code
    return response
